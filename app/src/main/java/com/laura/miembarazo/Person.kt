package com.laura.miembarazo

import android.os.Parcel
import android.os.Parcelable

class Person(
    var name: String?,
    var dateFUM: String?,
    var dateFPP: String?,
    var daysOfPregnancy: Long,
    var weeksOfPregnancy: Int,
    var addDaysofPregnancy: Int,
    var thisWeekDateInit: String?,
    var thisWeekDateEnd: String?,) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readLong(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(dateFUM)
        parcel.writeString(dateFPP)
        parcel.writeLong(daysOfPregnancy)
        parcel.writeInt(weeksOfPregnancy)
        parcel.writeInt(addDaysofPregnancy)
        parcel.writeString(thisWeekDateInit)
        parcel.writeString(thisWeekDateEnd)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Person> {
        override fun createFromParcel(parcel: Parcel): Person {
            return Person(parcel)
        }

        override fun newArray(size: Int): Array<Person?> {
            return arrayOfNulls(size)
        }
    }
}