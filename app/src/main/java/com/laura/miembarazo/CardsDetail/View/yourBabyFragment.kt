package com.laura.miembarazo.CardsDetail.View

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.laura.miembarazo.R
import com.laura.miembarazo.databinding.FragmentYourBabyBinding

class yourBabyFragment : Fragment() {

    lateinit var binding : FragmentYourBabyBinding
    private val args : yourBabyFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        val animation = TransitionInflater.from(requireContext()).inflateTransition(R.transition.transition_shared)
        sharedElementEnterTransition = animation
        sharedElementReturnTransition = animation
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentYourBabyBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.yourBabyDetailCardTitle.text = args.dataObject.yourBabyTxt
        binding.yourBabyImageFontFromDatabase.text = args.dataObject.yourBabyImgFont
        binding.yourBabyDetailCardContent.text = args.dataObject.yourBabyDescription

        Glide.with(requireContext())
            .asBitmap()
            .transform(CenterCrop(), RoundedCorners(context?.resources?.getDimensionPixelSize(R.dimen.cornerRadius)!!))
            .load(args.dataObject.yourBabyImg)
            .into(binding.yourBabyDetailImg)

        //binding.returnChevron.setOnClickListener { activity?.onBackPressed() }
    }
}