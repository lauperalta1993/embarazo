package com.laura.miembarazo.CardsDetail.View

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.laura.miembarazo.R
import com.laura.miembarazo.databinding.FragmentMedicalBinding

class medicalFragment : Fragment() {

    lateinit var binding : FragmentMedicalBinding
    private val args : medicalFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        val animation = TransitionInflater.from(requireContext()).inflateTransition(R.transition.transition_shared)
        sharedElementEnterTransition = animation
        sharedElementReturnTransition = animation
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMedicalBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.medicalDetailCardTitle.text = args.dataObject.medicalRecommendationsTxt
        binding.medicalImageFontFromDatabase.text = args.dataObject.medicalRecommendationsImgFont
        binding.medicalDetailCardContent.text = args.dataObject.medicalRecommendationsDescription

        Glide.with(requireContext())
            .asBitmap()
            .transform(CenterCrop(), RoundedCorners(context?.resources?.getDimensionPixelSize(R.dimen.cornerRadius)!!))
            .load(args.dataObject.yourBabyImg)
            .into(binding.medicalDetailImg)
    }
}