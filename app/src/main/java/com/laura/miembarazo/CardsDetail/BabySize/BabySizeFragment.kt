package com.laura.miembarazo.CardsDetail.BabySize

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.laura.miembarazo.CardsDetail.View.medicalFragmentArgs
import com.laura.miembarazo.DataBase.Person.ROOMdb.PersonRoomDatabase
import com.laura.miembarazo.Profile.ViewModel.ProfileViewModel
import com.laura.miembarazo.Profile.ViewModelFactory.ProfileViewModelFactory
import com.laura.miembarazo.R
import com.laura.miembarazo.databinding.FragmentBabySizeBinding

class babySizeFragment : Fragment() {


    lateinit var binding: FragmentBabySizeBinding
    private val args : babySizeFragmentArgs by navArgs()
    lateinit var viewModel: BabySizeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBabySizeBinding.inflate(layoutInflater)

        //inicializo ViewModel + viewModelFactory
        val application = requireNotNull(this.activity).application

        val dataSource = PersonRoomDatabase.getDatabase(application).personDAO

        val viewModelFactory = BabySizeViewModelFactory(dataSource, application)

        viewModel = ViewModelProvider(this, viewModelFactory).get(BabySizeViewModel::class.java)


        //retorno vista
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.babySizeDetailTitle.text = "Tamaño del bebe en la semana " + args.dataObject.week
        binding.contentHeader.text = args.dataObject.babySizeHeader
        binding.contentDetail.text = args.dataObject.babySizeDetail

        Glide.with(requireContext())
            .asBitmap()
            .transform(CenterCrop(), RoundedCorners(context?.resources?.getDimensionPixelSize(R.dimen.cornerRadius)!!))
            .load(args.dataObject.babySizeImg)
            .into(binding.detailImg)

        binding.imageFontFromDatabase.text = args.dataObject.babySizeImgFont

        binding.babyLengthData.text = args.dataObject.size
        binding.babyWeightData.text = args.dataObject.weight
    }

}
