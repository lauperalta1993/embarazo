package com.laura.miembarazo.CardsDetail.Model

import android.os.Parcel
import android.os.Parcelable

data class DataObject(
    val week: Int?,
    val trimester: Int?,
    val size: String?,
    val weight: String?,
    val featuredTxt: String?,
    val featuredImg: String?,
    val featuredImgFont: String?,
    val yourBabyTxt: String?,
    val yourBabyImg: String?,
    val yourBabyImgFont: String?,
    val yourBabyDescription: String?,
    val yourBodyTxt: String?,
    val yourBodyImg: String?,
    val yourBodyImgFont: String?,
    val yourBodyDescription: String?,
    val babySizeTxt: String?,
    val babySizeImg: String?,
    val babySizeImgFont: String?,
    val babySizeHeader: String?,
    val babySizeDetail: String?,
    val medicalRecommendationsTxt: String?,
    val medicalRecommendationsImg: String?,
    val medicalRecommendationsImgFont: String?,
    val medicalRecommendationsDescription: String?,
    val nextCheckUpTxt: String?,
    val nextCheckUpImg: String?,
    val nextCheckUpImgFont: String?,
    val nextCheckUpDescription: String?,
    val babyBDP: Int?,
    val babyLF: Int?,
    val PAFminP5: Int?,
    val PAFmaxP90: Int?,
    val momWeightMinP25: Int?,
    val momWeightMaxP90: Int?,
    val momUterineMinP10: Int?,
    val momUterineMaxP90: Int?,
    val momContractions: Int?,
    val bloodPresureSMax: Int?,
    val bloodPresureSMin: Int?,
    val bloodPresureDMax: Int?,
    val bloodPresureDMin: Int?,
): Parcelable{
    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(week)
        parcel.writeValue(trimester)
        parcel.writeString(size)
        parcel.writeString(weight)
        parcel.writeString(featuredTxt)
        parcel.writeString(featuredImg)
        parcel.writeString(featuredImgFont)
        parcel.writeString(yourBabyTxt)
        parcel.writeString(yourBabyImg)
        parcel.writeString(yourBabyImgFont)
        parcel.writeString(yourBabyDescription)
        parcel.writeString(yourBodyTxt)
        parcel.writeString(yourBodyImg)
        parcel.writeString(yourBodyImgFont)
        parcel.writeString(yourBodyDescription)
        parcel.writeString(babySizeTxt)
        parcel.writeString(babySizeImg)
        parcel.writeString(babySizeImgFont)
        parcel.writeString(babySizeHeader)
        parcel.writeString(babySizeDetail)
        parcel.writeString(medicalRecommendationsTxt)
        parcel.writeString(medicalRecommendationsImg)
        parcel.writeString(medicalRecommendationsImgFont)
        parcel.writeString(medicalRecommendationsDescription)
        parcel.writeString(nextCheckUpTxt)
        parcel.writeString(nextCheckUpImg)
        parcel.writeString(nextCheckUpImgFont)
        parcel.writeString(nextCheckUpDescription)
        parcel.writeValue(babyBDP)
        parcel.writeValue(babyLF)
        parcel.writeValue(PAFminP5)
        parcel.writeValue(PAFmaxP90)
        parcel.writeValue(momWeightMinP25)
        parcel.writeValue(momWeightMaxP90)
        parcel.writeValue(momUterineMinP10)
        parcel.writeValue(momUterineMaxP90)
        parcel.writeValue(momContractions)
        parcel.writeValue(bloodPresureSMax)
        parcel.writeValue(bloodPresureSMin)
        parcel.writeValue(bloodPresureDMax)
        parcel.writeValue(bloodPresureDMin)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DataObject> {
        override fun createFromParcel(parcel: Parcel): DataObject {
            return DataObject(parcel)
        }

        override fun newArray(size: Int): Array<DataObject?> {
            return arrayOfNulls(size)
        }
    }
}