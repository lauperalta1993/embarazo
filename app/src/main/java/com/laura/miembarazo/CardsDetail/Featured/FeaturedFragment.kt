package com.laura.miembarazo.CardsDetail.Featured

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.laura.miembarazo.CardsDetail.BabySize.BabySizeViewModel
import com.laura.miembarazo.CardsDetail.BabySize.BabySizeViewModelFactory
import com.laura.miembarazo.DataBase.Person.ROOMdb.PersonRoomDatabase
import com.laura.miembarazo.R
import com.laura.miembarazo.databinding.FragmentFeaturedBinding

class FeaturedFragment : Fragment() {

    lateinit var binding: FragmentFeaturedBinding
    private val args: FeaturedFragmentArgs by navArgs()

    lateinit var viewModel: FeaturedViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        binding = FragmentFeaturedBinding.inflate(layoutInflater)

        //inicializo ViewModel + viewModelFactory
        val application = requireNotNull(this.activity).application

        val dataSource = PersonRoomDatabase.getDatabase(application).personDAO

        val viewModelFactory = FeaturedViewModelFactory(dataSource, application)

        viewModel = ViewModelProvider(this, viewModelFactory).get(FeaturedViewModel::class.java)

        viewModel.personData.observe(viewLifecycleOwner, Observer { newPersonData ->
            binding.FPPData.text = newPersonData?.dateFPP
        })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setBabyData()
        setBabyInfo()

        setMomData()
        setMomInfo()

    }

    fun setBabyData(){
        binding.FPPData.text = viewModel.personData.value?.dateFPP
        binding.babyTrimesterData.text = args.dataObject.trimester.toString()
        binding.babyAgeData.text = ((args.dataObject.week!!)-2).toString() + " semanas"
        binding.babyBDPData.text = args.dataObject.babyBDP.toString()
        binding.babyLFData.text = args.dataObject.babyLF.toString()
        binding.babyWeightData.text = args.dataObject.weight
        binding.babySizeData.text = args.dataObject.size
        binding.babyPAFDatamin.text = args.dataObject.PAFminP5.toString()
        binding.babyPAFDatamax.text = args.dataObject.PAFmaxP90.toString()
    }

    fun setBabyInfo(){
        binding.FPPInfoIcon.setOnClickListener { Toast.makeText(requireContext(), getString(R.string.fppInfo), Toast.LENGTH_SHORT).show() }
        binding.babyTrimesterInfoIcon.setOnClickListener { Toast.makeText(requireContext(), getString(R.string.trimesterInfo), Toast.LENGTH_SHORT).show() }
        binding.ageInfoIcon.setOnClickListener { Toast.makeText(requireContext(), getString(R.string.ageInfo), Toast.LENGTH_SHORT).show() }
        binding.BDPInfoIcon.setOnClickListener { Toast.makeText(requireContext(), getString(R.string.BDPInfo), Toast.LENGTH_SHORT).show() }
        binding.LFInfoIcon.setOnClickListener { Toast.makeText(requireContext(), getString(R.string.LFInfo), Toast.LENGTH_SHORT).show() }
        binding.weightInfoIcon.setOnClickListener { Toast.makeText(requireContext(), getString(R.string.weightInfo), Toast.LENGTH_SHORT).show() }
        binding.sizeInfoIcon.setOnClickListener { Toast.makeText(requireContext(), getString(R.string.sizeInfo), Toast.LENGTH_SHORT).show() }
        binding.PAFInfoIconMin.setOnClickListener { Toast.makeText(requireContext(), getString(R.string.PAFminInfo), Toast.LENGTH_SHORT).show() }
        binding.PAFInfoIconMax.setOnClickListener { Toast.makeText(requireContext(), getString(R.string.PAFmaxInfo), Toast.LENGTH_SHORT).show() }
    }

    fun setMomData(){
        binding.momWeightDatamin.text = args.dataObject.momWeightMinP25.toString() + "kg"
        binding.momWeightDatamax.text = args.dataObject.momWeightMaxP90.toString() + "kg"
        binding.uterineHeightDatamin.text = args.dataObject.momUterineMinP10.toString()
        binding.uterineHeightDatamax.text = args.dataObject.momUterineMaxP90.toString()
        binding.contractionsData.text = args.dataObject.momContractions.toString()
        binding.sistDatamax.text = args.dataObject.bloodPresureSMax.toString()
        binding.sistDatamin.text = args.dataObject.bloodPresureSMin.toString()
        binding.diastDatamax.text = args.dataObject.bloodPresureDMax.toString()
        binding.diastDatamin.text = args.dataObject.bloodPresureDMin.toString()
    }

    fun setMomInfo(){
        binding.momWeightInfoIconMin.setOnClickListener { Toast.makeText(requireContext(), getString(R.string.momWeightMinInfo), Toast.LENGTH_SHORT).show() }
        binding.momWeightInfoIconMax.setOnClickListener { Toast.makeText(requireContext(), getString(R.string.momWeightMaxInfo), Toast.LENGTH_SHORT).show() }
        binding.uterineHeightInfoIconMin.setOnClickListener { Toast.makeText(requireContext(), getString(R.string.momUterineMinInfo), Toast.LENGTH_SHORT).show() }
        binding.uterineHeightInfoIconMax.setOnClickListener { Toast.makeText(requireContext(), getString(R.string.momUterineMaxInfo), Toast.LENGTH_SHORT).show() }
        binding.contractionsInfoIcon.setOnClickListener { Toast.makeText(requireContext(), getString(R.string.contractionsInfo), Toast.LENGTH_SHORT).show() }
        binding.bloodPresureInfoIcon.setOnClickListener { Toast.makeText(requireContext(), getString(R.string.bloodPresureInfo), Toast.LENGTH_SHORT).show() }

    }

}