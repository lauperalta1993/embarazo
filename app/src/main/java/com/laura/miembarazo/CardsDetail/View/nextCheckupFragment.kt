package com.laura.miembarazo.CardsDetail.View

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.laura.miembarazo.R
import com.laura.miembarazo.databinding.FragmentMedicalBinding
import com.laura.miembarazo.databinding.FragmentNextCheckupBinding

class nextCheckupFragment : Fragment() {


    lateinit var binding : FragmentNextCheckupBinding
    private val args : nextCheckupFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        val animation = TransitionInflater.from(requireContext()).inflateTransition(R.transition.transition_shared)
        sharedElementEnterTransition = animation
        sharedElementReturnTransition = animation
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNextCheckupBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.checkUpDetailCardTitle.text = args.dataObject.nextCheckUpTxt
        binding.checkUpImageFontFromDatabase.text = args.dataObject.nextCheckUpImgFont
        binding.checkUpDetailCardContent.text = args.dataObject.nextCheckUpDescription

        Glide.with(requireContext())
            .asBitmap()
            .transform(CenterCrop(), RoundedCorners(context?.resources?.getDimensionPixelSize(R.dimen.cornerRadius)!!))
            .load(args.dataObject.nextCheckUpImg)
            .into(binding.checkUpDetailImg)
    }
}