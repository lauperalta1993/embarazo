package com.laura.miembarazo.CardsDetail.Featured

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.laura.miembarazo.DataBase.Person.DAO.PersonDAO
import com.laura.miembarazo.DataBase.Person.Entity.PersonEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FeaturedViewModel(val database: PersonDAO,
                        application: Application
): AndroidViewModel(application) {

    private val _personData = MutableLiveData<PersonEntity?>()
    val personData: MutableLiveData<PersonEntity?>
        get() = _personData

    init{
        getPerson()
    }

    fun getPerson() {
        viewModelScope.launch {
            _personData.value = withContext(Dispatchers.IO){database.getPerson()}!!
            Log.d("personInfo",personData.toString())
        }
    }
}