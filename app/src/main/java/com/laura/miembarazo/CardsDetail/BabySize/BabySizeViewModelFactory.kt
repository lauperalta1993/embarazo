package com.laura.miembarazo.CardsDetail.BabySize

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.laura.miembarazo.DataBase.Person.DAO.PersonDAO

class BabySizeViewModelFactory  (  private val dataSource: PersonDAO, private val application: Application) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BabySizeViewModel::class.java)) {
            return BabySizeViewModel(dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}