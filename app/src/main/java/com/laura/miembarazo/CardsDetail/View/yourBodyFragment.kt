package com.laura.miembarazo.CardsDetail.View

import android.os.Bundle
import android.transition.TransitionInflater
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.laura.miembarazo.R
import com.laura.miembarazo.databinding.FragmentYourBodyBinding

class yourBodyFragment : Fragment() {

    lateinit var binding : FragmentYourBodyBinding
    private val args : yourBodyFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)

        val animation = TransitionInflater.from(requireContext()).inflateTransition(R.transition.transition_shared)

        sharedElementEnterTransition = animation
        sharedElementReturnTransition = animation
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentYourBodyBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.yourBodyDetailCardTitle.text = args.dataObject.yourBabyTxt
        binding.yourBodyImageFontFromDatabase.text = args.dataObject.yourBabyImgFont
        binding.yourBodyDetailCardContent.text = args.dataObject.yourBodyDescription

        Glide.with(requireContext())
            .asBitmap()
            .transform(CenterCrop(), RoundedCorners(context?.resources?.getDimensionPixelSize(R.dimen.cornerRadius)!!))
            .load(args.dataObject.yourBodyImg)
            .into(binding.yourBodyDetailImg)

        //binding.returnChevron.setOnClickListener { activity?.onBackPressed() }
    }
}
