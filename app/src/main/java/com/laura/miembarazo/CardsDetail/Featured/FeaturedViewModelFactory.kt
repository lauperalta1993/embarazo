package com.laura.miembarazo.CardsDetail.Featured

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.laura.miembarazo.DataBase.Person.DAO.PersonDAO

class FeaturedViewModelFactory  (private val dataSource: PersonDAO, private val application: Application) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FeaturedViewModel::class.java)) {
            return FeaturedViewModel(dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}