package com.laura.miembarazo.Login.ViewModel

import android.app.Application
import android.icu.util.Calendar
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.*
import com.laura.miembarazo.DataBase.Person.DAO.PersonDAO
import com.laura.miembarazo.DataBase.Person.Entity.PersonEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class LoginViewModel(val database: PersonDAO,
                     application: Application
): AndroidViewModel(application) {

    private val _personName = MutableLiveData<String>()
    val personName : LiveData<String>
        get() = _personName

    private val _personDateFUM = MutableLiveData<Date>()
    val personDateFUM : LiveData<Date>
        get() = _personDateFUM

    private val _personFPP  = MutableLiveData<Calendar>()
    val personFPP : LiveData<Calendar>
        get() = _personFPP

    private val _personDaysOfPregnancy = MutableLiveData<Long>()
    val personDaysOfPregnancy : LiveData<Long>
        get() = _personDaysOfPregnancy

    //lateinit var person: PersonEntity

//    fun setPersonDB(personData: PersonEntity)
//    {
//        viewModelScope.launch {
//            withContext(Dispatchers.IO){
//                database.insert(person = personData)
//                Log.d("persondata","creada")
//            }
//        }
//    }

    fun clearDB(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                database.clear()
                Log.d("clear","db clear")
            }
        }
    }



    @JvmName("setDateFUM1")
    fun setDateFUM(date: Date){
        _personDateFUM.value = date
    }

    fun setName(Name: String){
        _personName.value = Name
    }
    
    @RequiresApi(Build.VERSION_CODES.N)
    fun setFPP(selectDate: Calendar){
        _personFPP.value = selectDate
        _personFPP.value?.add(Calendar.DATE,280)
    }
    
    fun setDaysOfPregnancy() {
        _personDaysOfPregnancy.value = Math.floor((Date().time.toDouble() - _personDateFUM.value?.time!!.toDouble()) / 86400000).toLong()
    }


    @RequiresApi(Build.VERSION_CODES.N)
    fun getFPP(): Calendar {
        //get FPP
        return _personFPP.value!!
    }

    @JvmName("getDaysOfPregnancy1")
    fun getDaysOfPregnancy() : Long {
        //get number of days of pregnancy since dateFUM
        return _personDaysOfPregnancy.value!!
    }

    fun getWeeksOfPregnancy():Int{
        return (_personDaysOfPregnancy.value?.div(7))!!.toInt()
    }

    fun getAddDaysOfPregnancy():Int{
        return (_personDaysOfPregnancy.value?.rem(7))!!.toInt()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun getThisWeekInit(): Calendar? {
        val thisWeekInit = Calendar.getInstance()
        thisWeekInit?.add(Calendar.DATE,((-1*getAddDaysOfPregnancy())))
        return thisWeekInit
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun getThisWeekEnd(): Calendar? {
        val thisWeekEnd = Calendar.getInstance()
        thisWeekEnd?.add(Calendar.DATE,((7-getAddDaysOfPregnancy())))
        return thisWeekEnd
    }

    fun checkpregnancy(): Boolean {
        return if (_personDaysOfPregnancy.value?.toInt()  in 15..280) {
            Log.d("fpp", _personDaysOfPregnancy.value.toString())
            true
        } else{
            Log.d("fppp", _personDaysOfPregnancy.value.toString())
            false
        }
    }

    private fun setPerson(){

    }


}