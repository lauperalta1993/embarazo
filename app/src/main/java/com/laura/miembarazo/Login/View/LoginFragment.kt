package com.laura.miembarazo.Login.View

import android.app.DatePickerDialog
import android.content.Intent
import android.icu.util.Calendar
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.laura.miembarazo.DataBase.Person.Entity.PersonEntity
import com.laura.miembarazo.DataBase.Person.ROOMdb.PersonRoomDatabase
import com.laura.miembarazo.Login.ViewModel.LoginViewModel
import com.laura.miembarazo.Login.ViewModelFactory.LoginViewModelFactory
import com.laura.miembarazo.Person
import com.laura.miembarazo.Profile.ViewModel.ProfileViewModel
import com.laura.miembarazo.Profile.ViewModelFactory.ProfileViewModelFactory
import com.laura.miembarazo.R
import com.laura.miembarazo.Weeks.View.WeekActivity
import com.laura.miembarazo.databinding.FragmentLoginBinding
import java.text.SimpleDateFormat
import java.util.*


class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding
    private lateinit var viewModel : LoginViewModel
    lateinit var selectDate: Calendar

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // inflate the layout
        binding = FragmentLoginBinding.inflate(layoutInflater)


        //inicializo ViewModel + viewModelFactory
        val application = requireNotNull(this.activity).application

        val dataSource = PersonRoomDatabase.getDatabase(application).personDAO

        val viewModelFactory = LoginViewModelFactory(dataSource, application)

        viewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)

      //Nunca lo asigna a viewModel.personname por eso nunca lo guarda pero esto no anda. Ver donde puedo asignarlo. Ahora anda pero no me gusta setNamee
        viewModel.personName.observe(viewLifecycleOwner, Observer { newName ->
            binding.inputName.editText?.setText(newName)
        })

        viewModel.personDateFUM.observe(viewLifecycleOwner, Observer { newDatefum ->
            binding.inputFUM.text = formatDate.format(newDatefum)
        })

        viewModel.personFPP.observe(viewLifecycleOwner, Observer { newDate ->
            binding.inputFPP.editText?.setText (formatDate.format(newDate.time).toString())
        })

        binding.loginToolbar.inflateMenu(R.menu.menu_login)

         return binding.root
     }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.loginToolbar.setOnMenuItemClickListener {
            when(it.itemId){
                R.id.menuLogin_info -> {
                    Toast.makeText(requireContext(),"info",Toast.LENGTH_SHORT).show()
                    true
                }
                else -> {false}
            }
        }

        binding.inputFUM.setOnClickListener(View.OnClickListener{
            DatePickerDialog(requireContext(),DatePickerDialog.OnDateSetListener { datePicker, year, month, dayOfMonth ->
                selectDate = Calendar.getInstance()
                selectDate.set(Calendar.YEAR,year)
                selectDate.set(Calendar.MONTH,month)
                selectDate.set(Calendar.DAY_OF_MONTH,dayOfMonth)
                viewModel.setDateFUM(selectDate.time)
                viewModel.setFPP(selectDate)
                binding.inputFUM.setText(formatDate.format(viewModel.personDateFUM.value?.time))
                binding.inputFPP.editText?.setText (formatDate.format(viewModel.getFPP().time).toString())
                viewModel.setDaysOfPregnancy()
            }
            ,Calendar.getInstance().get(Calendar.YEAR),Calendar.getInstance().get(Calendar.MONTH),Calendar.getInstance().get(Calendar.DAY_OF_MONTH)).show()
            // No me gusta esto
            viewModel.setName(binding.inputName.editText?.text.toString())
        })


        binding.btnLogin.setOnClickListener {
            if(binding.inputName.editText?.text.toString().isNotEmpty() && binding.inputFUM.text.toString().isNotEmpty()) {
                if (viewModel.checkpregnancy()) {
                    val person = Person(
                        binding.inputName.editText?.text.toString(),
                        binding.inputFUM.text.toString(),
                        formatDate.format(viewModel.getFPP().time).toString(),
                        viewModel.getDaysOfPregnancy(),
                        viewModel.getWeeksOfPregnancy(),
                        viewModel.getAddDaysOfPregnancy(),
                        formatDate.format(viewModel.getThisWeekInit()?.time).toString(),
                        formatDate.format(viewModel.getThisWeekEnd()?.time).toString()
                    )


//                    var personData: PersonEntity = PersonEntity(
//                        name = binding.inputName.editText?.text.toString(),
//                        dateFUM = person.dateFUM!!,
//                        dateFPP = person.dateFPP!!,
//                        daysOfPregnancy = person.daysOfPregnancy,
//                        weeksOfPregnancy = person.weeksOfPregnancy,
//                        addDaysofPregnancy = person.addDaysofPregnancy,
//                        thisWeekDateInit = person.thisWeekDateInit!!,
//                        thisWeekDateEnd = person.thisWeekDateEnd!!)
//
//                    viewModel.setPersonDB(personData)



                    val intent = Intent(requireActivity(), WeekActivity :: class.java)
                    intent.putExtra("person",person)
                    Log.d("name", person.name!!)
                    viewModel.clearDB()
                    startActivity(intent)


                } else {
                    Snackbar.make(
                        view.findViewById(R.id.loginLayout),
                        R.string.invalidDate,
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            } else{
                Snackbar.make(view.findViewById(R.id.loginLayout),
                    R.string.emptyField,
                    Snackbar.LENGTH_LONG).show()
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    val formatDate = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        SimpleDateFormat ("dd/MM/YYYY", Locale.US)
    } else {
        TODO("VERSION.SDK_INT < N")
    }
}




