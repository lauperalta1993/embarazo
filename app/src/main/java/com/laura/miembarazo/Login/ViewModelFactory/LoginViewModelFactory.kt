package com.laura.miembarazo.Login.ViewModelFactory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.laura.miembarazo.DataBase.Person.DAO.PersonDAO
import com.laura.miembarazo.Login.ViewModel.LoginViewModel


class LoginViewModelFactory(
    private val dataSource: PersonDAO,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}