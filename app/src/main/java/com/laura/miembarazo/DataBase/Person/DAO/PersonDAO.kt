package com.laura.miembarazo.DataBase.Person.DAO


import androidx.room.*
import com.laura.miembarazo.DataBase.Person.Entity.PersonEntity

@Dao
interface PersonDAO {

    @Insert
    suspend fun insert (person: PersonEntity)

    @Update
    suspend fun update(person: PersonEntity)

    @Query("DELETE FROM person_information")
    suspend fun clear()

    @Query("SELECT * FROM person_information")
    suspend fun getAll(): PersonEntity

    @Query("SELECT * from person_information WHERE personId = :key")
    fun get(key: Int): PersonEntity

    @Query("SELECT * FROM person_information ORDER BY personId DESC")
    suspend fun getPerson(): PersonEntity
}