package com.laura.miembarazo.DataBase.Person.ROOMdb

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.laura.miembarazo.DataBase.Person.DAO.PersonDAO
import com.laura.miembarazo.DataBase.Person.Entity.PersonEntity
import okio.ByteString.Companion.toByteString

@Database (entities = [PersonEntity::class], version= 1, exportSchema = false)
abstract class PersonRoomDatabase: RoomDatabase() {
    abstract val personDAO : PersonDAO

    companion object{
        @Volatile
        private var INSTANCE: PersonRoomDatabase?=null

        fun getDatabase(context: Context): PersonRoomDatabase {
            synchronized(this) {

                var instance = INSTANCE


                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        PersonRoomDatabase::class.java,
                        "sleep_history_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    Log.d("dbcreada", "person")

                    INSTANCE = instance
                }


                return instance
            }
        }
    }
}