package com.laura.miembarazo.DataBase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pregnancy_week_data_table")
class WeekData (
    @PrimaryKey(autoGenerate = false)
    var weekId: Long = 0L,

    @ColumnInfo
    val trimester: Int = 0,
    @ColumnInfo
    val size: String,
    @ColumnInfo
    val weight: String,
    @ColumnInfo
    val featuredTxt: String,
    @ColumnInfo
    val featuredImg: String,
    @ColumnInfo
    val featuredImgFont: String,
    @ColumnInfo
    val yourBabyTxt: String,
    @ColumnInfo
    val yourBabyImg: String?,
    @ColumnInfo
    val yourBabyImgFont: String?,
    @ColumnInfo
    val yourBabyDescription: String?,
    @ColumnInfo
    val yourBodyTxt: String?,
    @ColumnInfo
    val yourBodyImg: String?,
    @ColumnInfo
    val yourBodyImgFont: String,
    @ColumnInfo
    val yourBodyDescription: String,
    @ColumnInfo
    val babySizeTxt: String,
    @ColumnInfo
    val babySizeImg: String,
    @ColumnInfo
    val babySizeImgFont: String,
    @ColumnInfo
    val babySizeHeader: String,
    @ColumnInfo
    val babySizeDetail: String,
    @ColumnInfo
    val medicalRecommendationsTxt: String,
    @ColumnInfo
    val medicalRecommendationsImg: String,
    @ColumnInfo
    val medicalRecommendationsImgFont: String,
    @ColumnInfo
    val medicalRecommendationsDescription: String,
    @ColumnInfo
    val nextCheckUpTxt: String,
    @ColumnInfo
    val nextCheckUpImg: String,
    @ColumnInfo
    val nextCheckUpImgFont: String,
    @ColumnInfo
    val nextCheckUpDescription: String,
    @ColumnInfo
    val babyBDP: Int= 0,
    @ColumnInfo
    val babyLF: Int= 0,
    @ColumnInfo
    val PAFminP5: Int= 0,
    @ColumnInfo
    val PAFmaxP90: Int= 0,
    @ColumnInfo
    val momWeightMinP25: Int= 0,
    @ColumnInfo
    val momWeightMaxP90: Int= 0,
    @ColumnInfo
    val momUterineMinP10: Int= 0,
    @ColumnInfo
    val momUterineMaxP90: Int= 0,
    @ColumnInfo
    val momContractions: Int= 0,
    @ColumnInfo
    val bloodPresureSMax: Int= 0,
    @ColumnInfo
    val bloodPresureSMin: Int= 0,
    @ColumnInfo
    val bloodPresureDMax: Int= 0,
    @ColumnInfo
    val bloodPresureDMin: Int= 0,
    ){}