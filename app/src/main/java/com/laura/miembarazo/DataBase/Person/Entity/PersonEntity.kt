package com.laura.miembarazo.DataBase.Person.Entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "person_information")
data class PersonEntity (

    @PrimaryKey(autoGenerate = true) var personId: Int=0,
    @ColumnInfo (name= "person_name") var name: String,
    @ColumnInfo (name= "person_date_FUM") var dateFUM: String,
    @ColumnInfo (name= "person_date_FPP") var dateFPP: String,
    @ColumnInfo (name= "person_daysOfPregnancy") var daysOfPregnancy: Long= 0L,
    @ColumnInfo (name= "person_weeksOfPregnancy") var weeksOfPregnancy: Int= -1,
    @ColumnInfo (name= "person_addDaysOfPregnancy") var addDaysofPregnancy: Int= 0,
    @ColumnInfo (name= "person_thisWeekDateInit") var thisWeekDateInit: String,
    @ColumnInfo (name= "person_thisWeekDateEnd") var thisWeekDateEnd: String,
    )