package com.laura.miembarazo.Weeks

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.laura.miembarazo.CardsDetail.Model.DataObject
import com.laura.miembarazo.DataBase.Person.DAO.PersonDAO
import com.laura.miembarazo.DataBase.Person.Entity.PersonEntity
import com.laura.miembarazo.DataBase.Person.ROOMdb.PersonRoomDatabase

import com.laura.miembarazo.Weeks.Model.ReadingsItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream

class weekViewModel(val database: PersonDAO, application: Application): AndroidViewModel(application){

    //current Week: Semana actual de embarazo a la fecha. Se asigna a partir de args.person
    private val _currentWeek = MutableLiveData<Int>()
    val currentWeek: LiveData<Int>
        get() = _currentWeek

    //addDays: dias adicionales a la semana de embarazo a la fecha. Se asigna a partir de args.person
    private val _addDays = MutableLiveData<Int>()
    val addDays: LiveData<Int>
        get() = _addDays

    //Selected week: semana seleccionada. Puede o no coincidir con la semana de embarazo a la fecha. Varia con navegacion (chevrones).
    private val _selectedWeek = MutableLiveData<Int>()
    val selectedWeek: LiveData<Int>
        get() = _selectedWeek

    //DiffWeek : diferencia numerica entre la semana actual de embarazo a la fecha y la semana seleccionada
    private val _difWeek = MutableLiveData<Int>()
    val difWeek: LiveData<Int>
        get() = _difWeek

    //Arreglo de objetos obtenidos a partir de weekDatabase.json
    private val _jsonDataArray = MutableLiveData<JSONArray>()
    val jsonDataArray: LiveData<JSONArray>
        get() = _jsonDataArray

    private val _jsonDataWeekObj = MutableLiveData<JSONObject>()
    val jsonDataWeekObj: LiveData<JSONObject>
        get() = _jsonDataWeekObj

    //el indice es igual a la semana seleccionada  -2
    private val _jsonObjectIndex = MutableLiveData<Int>()
    val jsonObjectIndex: LiveData<Int>
        get() = _jsonObjectIndex

    //variables obtenidas a partir de readings database.json
    lateinit var readings: MutableList<ReadingsItem>


    private val _readinglist = MutableLiveData<MutableList<ReadingsItem>>()
    val readinglist: MutableLiveData<MutableList<ReadingsItem>>
        get() = _readinglist

    private val _itemId = MutableLiveData<Int>()
    val itemId : MutableLiveData<Int>
        get() = _itemId

    private val _dataObject = MutableLiveData<DataObject>()
    val dataObject : MutableLiveData <DataObject>
        get() = _dataObject

    private val _personData = MutableLiveData<PersonEntity?>()
    val personData: MutableLiveData<PersonEntity?>
        get() = _personData


   // init{getPerson()}

    fun setUp(context: Context) {
        getPerson()
        getJsonArrayWeek(context)
        getReadings(context)
        createDataObject()
    }

    fun setPersonDB(personData: PersonEntity)
    {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                database.insert(person = personData)
                Log.d("persondata","creada")
            }
        }
    }

    fun getPerson() {
        viewModelScope.launch {
            _personData.value = withContext(Dispatchers.IO){database.getPerson()}
            Log.d("personInfo",personData. value.toString())
        }
    }

    fun onReadingsItemClicked(id : Int ){
        _itemId.value = id
    }


    //esta mal esto
    fun getUrl(ID : Int): String? {
        for (i in 0 until readings.size - 1){
            if (readinglist.value?.get(i)?.ID == ID){
                return readinglist.value?.get(i)?.URL
            }
        }
        return null
    }


    fun setCurrentWeek(weekNumber: Int) {
        _currentWeek.value = weekNumber
        Log.d(jsonObjectIndex.value.toString(), "asign_current_week")
    }

    fun setAddDays(numberOfDays: Int) {
        _addDays.value = numberOfDays
    }

    //obtengo arreglo de datos a partir de weekDatabase.json
    fun getJsonArrayWeek(context: Context) {
        _readinglist.value = mutableListOf()
        var json: String? = null

        try {
            val inputStream: InputStream = context?.assets!!.open("weekDatabase.json")
            json = inputStream.bufferedReader().use { it.readText() }

            _jsonDataArray.value = JSONArray(json)
            getJsonObjectfromJsonArray()

        } catch (e: IOException) {
        }
    }

    fun getJsonObjectfromJsonArray() {

        when (jsonObjectIndex.value) {

            null -> {

                var jsonObj = jsonDataArray.value!!.getJSONObject(currentWeek.value!!.toInt() - 2)
                //JSONARRAY[i] -> jsonarray.getJsonObject[i]

                _jsonDataWeekObj.value = jsonObj
                _jsonObjectIndex.value = currentWeek.value!!.toInt() - 2
                _selectedWeek.value = currentWeek.value
                _difWeek.value = 0
                Log.d(jsonObjectIndex.toString(), "resetindex")

            }

            0 -> {
                var jsonObj = jsonDataArray.value!!.getJSONObject(selectedWeek.value!!.toInt() - 2)

                _jsonDataWeekObj.value = jsonObj
                _difWeek.value = selectedWeek.value!!.toInt().minus(currentWeek.value!!.toInt())
            }

            in 1..38 -> {
                var jsonObj = jsonDataArray.value!!.getJSONObject(selectedWeek.value!!.toInt() - 2)

                _jsonDataWeekObj.value = jsonObj
                _difWeek.value = selectedWeek.value!!.toInt().minus(currentWeek.value!!.toInt())

            }

            39 -> {
                var jsonObj = jsonDataArray.value!!.getJSONObject(selectedWeek.value!!.toInt() - 2)

                _jsonDataWeekObj.value = jsonObj
                _difWeek.value = selectedWeek.value!!.toInt().minus(currentWeek.value!!.toInt())

            }
        }
    }


    //defino el texto del encabezado
    fun setHeaderData(): Int {

        var status: Int = 0

        when(difWeek.value!!.toInt()){
            0 -> when (addDays.value) {
                1-> status = 3
                else -> status = 4
            }
            -1 -> status = 2
            in (-40)..(-2) -> status = 1
            1 -> status = 5
            in 2..40 -> status = 6
        }


//        if ((selectedWeek.value == currentWeek.value || difWeek.value!!.toInt() == 0) && (addDays.value == 1)) status =
//            1
//        else if ((selectedWeek.value == currentWeek.value || difWeek.value!!.toInt() == 0) && (addDays.value != 1)) status =
//            2
//        else if (difWeek.value!!.toInt() == -1) status = 3
//        else if (difWeek.value!!.toInt() < -1) status = 4
//        else if (difWeek.value!!.toInt() == 1) status = 5
//        else if (difWeek.value!!.toInt() > 1) status = 6

        Log.d(status.toString(), "status")
        return status
    }

    //se pasa como parametro al Featured Fragment
    fun setTrimester(weeks: Int): Int {
        var trimester = 0
        when (weeks) {
            in 1..13 -> trimester = 1
            in 14..25 -> trimester = 2
            in 26..40 -> trimester = 3
        }
        return trimester
    }


    fun navLeft() {

        if (jsonObjectIndex.value!!.toInt() in 1..38) {
            _jsonObjectIndex.value = _jsonObjectIndex.value!! - 1
            _selectedWeek.value = selectedWeek.value!! - 1
            getJsonObjectfromJsonArray()
            createDataObject()
            setReadingList()
        }
    }

    fun navRight() {

        if (jsonObjectIndex.value!!.toInt() in 0..37) {
            _jsonObjectIndex.value = _jsonObjectIndex.value!! + 1
            _selectedWeek.value = selectedWeek.value!! + 1
            Log.d((_selectedWeek.value).toString(), "right")
            getJsonObjectfromJsonArray()
            createDataObject()
            setReadingList()
        }

    }

    fun createDataObject() {
        _dataObject.value = DataObject(
            _jsonDataWeekObj.value!!.getInt("week"),
            setTrimester(selectedWeek.value!!),
            _jsonDataWeekObj.value!!.getString("size"),
            _jsonDataWeekObj.value!!.getString("weight"),
            _jsonDataWeekObj.value!!.getString("featuredTxt"),
            _jsonDataWeekObj.value!!.getString("featuredImg"),
            _jsonDataWeekObj.value!!.getString("featuredImgFont"),
            _jsonDataWeekObj.value!!.getString("yourBabyTxt"),
            _jsonDataWeekObj.value!!.getString("yourBabyImg"),
            _jsonDataWeekObj.value!!.getString("yourBabyImgFont"),
            _jsonDataWeekObj.value!!.getString("yourBabyDescription"),
            _jsonDataWeekObj.value!!.getString("yourBodyTxt"),
            _jsonDataWeekObj.value!!.getString("yourBodyImg"),
            _jsonDataWeekObj.value!!.getString("yourBodyImgFont"),
            _jsonDataWeekObj.value!!.getString("yourBodyDescription"),
            _jsonDataWeekObj.value!!.getString("babySizeTxt"),
            _jsonDataWeekObj.value!!.getString("babySizeImg"),
            _jsonDataWeekObj.value!!.getString("babySizeImgFont"),
            _jsonDataWeekObj.value!!.getString("babySizeHeader"),
            _jsonDataWeekObj.value!!.getString("babySizeDetail"),
            _jsonDataWeekObj.value!!.getString("medicalRecommendationsTxt"),
            _jsonDataWeekObj.value!!.getString("medicalRecommendationsImg"),
            _jsonDataWeekObj.value!!.getString("medicalRecommendationsImgFont"),
            _jsonDataWeekObj.value!!.getString("medicalRecommendationsDescription"),
            _jsonDataWeekObj.value!!.getString("nextCheckUpTxt"),
            _jsonDataWeekObj.value!!.getString("nextCheckUpImg"),
            _jsonDataWeekObj.value!!.getString("nextCheckUpImgFont"),
            _jsonDataWeekObj.value!!.getString("nextCheckUpDescription"),
            _jsonDataWeekObj.value!!.getInt("babyBDP"),
            _jsonDataWeekObj.value!!.getInt("babyLF"),
            _jsonDataWeekObj.value!!.getInt("PAFminP5"),
            _jsonDataWeekObj.value!!.getInt("PAFmaxP90"),
            _jsonDataWeekObj.value!!.getInt("momWeightMinP25"),
            _jsonDataWeekObj.value!!.getInt("momWeightMaxP90"),
            _jsonDataWeekObj.value!!.getInt("momUterineMinP10"),
            _jsonDataWeekObj.value!!.getInt("momUterineMaxP90"),
            _jsonDataWeekObj.value!!.getInt("momContractions"),
            _jsonDataWeekObj.value!!.getInt("bloodPresureSMax"),
            _jsonDataWeekObj.value!!.getInt("bloodPresureSMin"),
            _jsonDataWeekObj.value!!.getInt("bloodPresureDMax"),
            _jsonDataWeekObj.value!!.getInt("bloodPresureDMin"))
        Log.d("dataObject",_jsonDataWeekObj.value!!.getInt("week").toString())

    }

    //tomado de version anterior

    private fun getJsonReadings(
        context: Context,
        fileName: String = "readingsDatabase.json"
    ): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

    private fun getReadings(context: Context) {

        val jsonFileString = getJsonReadings(context, "readingsDatabase.json")
        val gson = Gson()
        val listReadings = object : TypeToken<List<ReadingsItem>>() {}.type
        readings = gson.fromJson(jsonFileString, listReadings)


//        for(i in 0 until readings.size-1)
//        {
//            if(readings[i].week == indexWeek) {
//                Log.d(readings[i].toString(),"asd")
//                readinglist.add((readings[i]))
//            }
//        }
//        return readings

    }

    fun setReadingList() {
        _readinglist.value?.clear()
        for (i in 0 until readings.size - 1) {
                if (readings[i].week == _selectedWeek.value!!.toInt()) {
                    _readinglist.value!!.add((readings[i]))
                }
            }
        }
}





