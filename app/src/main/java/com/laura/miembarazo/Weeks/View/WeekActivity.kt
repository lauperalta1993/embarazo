package com.laura.miembarazo.Weeks.View

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupWithNavController
import com.laura.miembarazo.databinding.ActivityWeekBinding
import com.laura.miembarazo.databinding.FragmentLoginBinding
import android.R
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import com.google.android.material.navigation.NavigationView


class WeekActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var binding: ActivityWeekBinding

    private lateinit var drawerLayout: DrawerLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        @Suppress("UNUSED_VARIABLE")
        val binding =
            DataBindingUtil.setContentView<ActivityWeekBinding>(this, com.laura.miembarazo.R.layout.activity_week)

        drawerLayout = binding.drawerLayoutWeek

        var toolbar : Toolbar = binding.weekActivityToolbar

        setSupportActionBar(toolbar)

        val navController = this.findNavController(com.laura.miembarazo.R.id.navigation_weekContainer)

        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout)

        NavigationUI.setupWithNavController(binding.navigationViewWeek, navController)



//        val appBarConfiguration = AppBarConfiguration(navController.graph)
//        binding.weekActivityToolbar.setupWithNavController(navController,appBarConfiguration





    }

    //manejo de eventos de click desde activity -
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        return when (item.itemId) {
//            com.laura.miembarazo.R.id.menu_profile -> {
//                // navigate to profile fragment
//                //Navigation.createNavigateOnClickListener(R.id.profileFragment,null)
//                val fragmentManager: FragmentManager = getFragmentManager()
//                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit()
//                true
//            }
//            com.laura.miembarazo.R.id.menu_profile -> {
//                // navigate to settings screen
//                true
//            }
//            else -> super.onOptionsItemSelected(item)
//        }
//    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId){
            com.laura.miembarazo.R.id.menu_profile -> {Navigation.createNavigateOnClickListener(com.laura.miembarazo.R.id.profileFragment,null)
                Toast.makeText(applicationContext,"profile",Toast.LENGTH_SHORT).show()}
            else -> Toast.makeText(applicationContext,"nothing",Toast.LENGTH_SHORT).show()
        }

        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = this.findNavController(com.laura.miembarazo.R.id.navigation_weekContainer)
        return NavigationUI.navigateUp(navController, drawerLayout)
    }

}