package com.laura.miembarazo.Weeks.Model

import android.os.Parcel
import android.os.Parcelable

data class ReadingsItem(
    val ID: Int,
    val URL: String?,
    val font: String?,
    val image: String?,
    val title: String?,
    val week: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(ID)
        parcel.writeString(URL)
        parcel.writeString(font)
        parcel.writeString(image)
        parcel.writeString(title)
        parcel.writeInt(week)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ReadingsItem> {
        override fun createFromParcel(parcel: Parcel): ReadingsItem {
            return ReadingsItem(parcel)
        }

        override fun newArray(size: Int): Array<ReadingsItem?> {
            return arrayOfNulls(size)
        }
    }
}