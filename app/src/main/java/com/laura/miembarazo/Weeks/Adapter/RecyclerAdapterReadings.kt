package com.laura.miembarazo.Weeks.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.laura.miembarazo.Weeks.Model.ReadingsItem
import com.laura.miembarazo.databinding.OtherReadingsItemBinding


//Declaración con constructor
class RecyclerAdapterReadings (val clickListener : ReadingsItemListener ): ListAdapter<ReadingsItem,RecyclerAdapterReadings.ViewHolder>(ReadingsItemDiffCallback()) {

    //Aquí atamos el ViewHolder
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder(val binding: OtherReadingsItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item : ReadingsItem, clickListener : ReadingsItemListener){
            binding.apply {
                root.setOnClickListener { clickListener.onClick(item) }
                binding.fontTxt.text = item.title
                Glide.with(binding.root.context)
                    .asBitmap()
                    .load(item.image)
                    .into(binding.fontImg)
            }
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = OtherReadingsItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class ReadingsItemDiffCallback : DiffUtil.ItemCallback<ReadingsItem>() {
    override fun areItemsTheSame(oldItem: ReadingsItem, newItem: ReadingsItem): Boolean {
        return oldItem.ID == newItem.ID
    }

    override fun areContentsTheSame(oldItem:ReadingsItem, newItem: ReadingsItem): Boolean {
        return oldItem == newItem
    }
}

class ReadingsItemListener(val clickListener: (ReadingsId: Int) -> Unit) {
    fun onClick(readings: ReadingsItem) = clickListener(readings.ID)
}

