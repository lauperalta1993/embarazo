package com.laura.miembarazo.Weeks.View

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.ImageView
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.laura.miembarazo.DataBase.Person.Entity.PersonEntity
import com.laura.miembarazo.DataBase.Person.ROOMdb.PersonRoomDatabase

import com.laura.miembarazo.Person
import com.laura.miembarazo.R
import com.laura.miembarazo.Weeks.Adapter.ReadingsItemListener
import com.laura.miembarazo.Weeks.Adapter.RecyclerAdapterReadings
import com.laura.miembarazo.Weeks.ViewModelFactory.WeekViewModelFactory
import com.laura.miembarazo.Weeks.weekViewModel
import com.laura.miembarazo.databinding.FragmentWeekBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

import java.lang.Math.abs


class week : Fragment() {

    lateinit var binding: FragmentWeekBinding
    lateinit var viewModel: weekViewModel

    //var person from bundle
    lateinit var person: Person
    //var personData from DB
    var personData: PersonEntity? = null

    var logged: Boolean = false
    var personId: Int? = 0

    private lateinit var drawerLayout: DrawerLayout


    var selectedWeek : Int? = null
    var difWeek : Int? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentWeekBinding.inflate(layoutInflater)

        val application = requireNotNull(this.activity).application

        val dataSource = PersonRoomDatabase.getDatabase(application).personDAO

        val viewModelFactory = WeekViewModelFactory(dataSource, application)

        viewModel = ViewModelProvider(this, viewModelFactory).get(weekViewModel::class.java)

        if(viewModel.personData.value?.personId == null){
            val bundle : Bundle? = requireActivity().intent.extras

            person = requireActivity().intent.getParcelableExtra("person")!!

            Log.d("name in week", person.name!!)

            var personData: PersonEntity = PersonEntity(
                name = person.name!!,
                dateFUM = person.dateFUM!!,
                dateFPP = person.dateFPP!!,
                daysOfPregnancy = person.daysOfPregnancy,
                weeksOfPregnancy = person.weeksOfPregnancy,
                addDaysofPregnancy = person.addDaysofPregnancy,
                thisWeekDateInit = person.thisWeekDateInit!!,
                thisWeekDateEnd = person.thisWeekDateEnd!!)

            viewModel.setPersonDB(personData)
            logged = true
            personId = viewModel.personData.value?.personId

        }else{
            person.name = viewModel.personData.value!!.name
            person.dateFUM = viewModel.personData.value!!.dateFUM
            person.dateFPP = viewModel.personData.value!!.dateFPP
            person.daysOfPregnancy = viewModel.personData.value!!.daysOfPregnancy
            person.weeksOfPregnancy = viewModel.personData.value!!.weeksOfPregnancy
            person.addDaysofPregnancy = viewModel.personData.value!!.addDaysofPregnancy
            person.thisWeekDateInit = viewModel.personData.value!!.thisWeekDateInit
            person.thisWeekDateEnd = viewModel.personData.value!!.thisWeekDateEnd

            Log.d("db not null",person.name.toString())
        }




//        viewModel.person = PersonEntity(
//            name = person.name!!,
//            dateFUM = person.dateFUM!!,
//            dateFPP = person.dateFPP!!,
//            daysOfPregnancy = person.daysOfPregnancy,
//            weeksOfPregnancy = person.weeksOfPregnancy,
//            addDaysofPregnancy = person.addDaysofPregnancy,
//            thisWeekDateInit = person.thisWeekDateInit!!,
//            thisWeekDateEnd = person.thisWeekDateEnd!!)


//        person  = viewModel.personData.value

        viewModel.setCurrentWeek(person!!.weeksOfPregnancy)
        viewModel.setAddDays(person!!.addDaysofPregnancy)

        viewModel.selectedWeek.observe(viewLifecycleOwner, Observer { newWeek ->
            selectedWeek = newWeek
        })

        viewModel.difWeek.observe(viewLifecycleOwner, Observer { newDifWeek ->
            difWeek = newDifWeek
        })

        val adapter = RecyclerAdapterReadings(ReadingsItemListener { readingsId ->
            viewModel.onReadingsItemClicked(readingsId)
        })
        binding.otherReadingslist.adapter = adapter


        viewModel.readinglist.observe(viewLifecycleOwner, Observer{
            it?.let {
                adapter.submitList(it)
            }
        })

        viewModel.itemId.observe(viewLifecycleOwner, Observer { itemid ->
            itemid?.let {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(
                    viewModel.getUrl(itemid)
                   ))
                startActivity(intent)
            }
        })

        //setHasOptionsMenu(true)

        binding.buttontest.setOnClickListener {
            var directions: NavDirections = weekDirections.actionWeekToProfileFragment()
            findNavController().navigate(directions)
        }


        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init(){
        viewModel.setUp(requireContext())
        //viewModel.initDB(requireActivity())
        viewModel.setReadingList()
        setHeader()
        setDataWeek()
        setOnClick()
        navigateWeeks()
        binding.otherReadingslist.isLayoutFrozen


    }

    private fun update(){
        setHeader()
        setDataWeek()
        viewModel.setReadingList()
        binding.otherReadingslist.adapter?.notifyDataSetChanged()
        binding.otherReadingslist.isLayoutFrozen

    }

    @SuppressLint("SetTextI18n")
    fun setHeader () {

        when (viewModel.setHeaderData()){
            1 -> {
                binding.weekNumber.text= getString(R.string.featured) + " " + viewModel.selectedWeek.value
                binding.weekDates.text = "(Hace ${abs(viewModel.difWeek.value!!)} Semanas)"
            }
            2 -> {
                binding.weekNumber.text= getString(R.string.featured) + " " + viewModel.selectedWeek.value
                binding.weekDates.text = "(Hace ${abs(viewModel.difWeek.value!!)} Semana)"
            }
            3 -> {
                binding.weekNumber.text = getString(R.string.pregnantAnnouncement) + " ${person!!.weeksOfPregnancy} " + getString(R.string.weeksOfPregnancy) + " ${person!!.addDaysofPregnancy} " + getString(R.string.day)
                binding.weekDates.text = "${person!!.thisWeekDateInit} - ${person!!.thisWeekDateEnd}"
            }

            4 -> {
                binding.weekNumber.text = getString(R.string.pregnantAnnouncement) + " ${person!!.weeksOfPregnancy} " + getString(R.string.weeksOfPregnancy) + " ${person!!.addDaysofPregnancy} " + getString(R.string.daysOfPregnancy)
                binding.weekDates.text = "${person!!.thisWeekDateInit} - ${person!!.thisWeekDateEnd}"
            }
            5 ->{
                binding.weekNumber.text= getString(R.string.featured) + " " + viewModel.selectedWeek.value
                binding.weekDates.text = "(Dentro de ${abs(viewModel.difWeek.value!!)} Semana)"
            }

            6 -> {
                binding.weekNumber.text= getString(R.string.featured) + " " + viewModel.selectedWeek.value
                binding.weekDates.text = "(Dentro de ${abs(viewModel.difWeek.value!!)} Semanas)"
            }
        }

    }

    fun setDataWeek(){

        if (viewModel.jsonDataWeekObj.value!!.getInt("week") == person!!.weeksOfPregnancy){
            binding.remainingDays.text = "FALTAN ${((7*40)- person!!.daysOfPregnancy)}"
        }else{
            binding.remainingDays.text = "-"
        }

        binding.babyWeight.text = viewModel.jsonDataWeekObj.value!!.getString("weight")
        binding.babyLength.text = viewModel.jsonDataWeekObj.value!!.getString("size")
        binding.cardTxt1.text = getString(R.string.weeks) + viewModel.jsonDataWeekObj.value!!.getInt("week").toString()

        setImages(viewModel.jsonDataWeekObj.value!!.getString("featuredImg"), binding.cardImg1)
        setImages(viewModel.jsonDataWeekObj.value!!.getString("yourBabyImg"), binding.cardImg2)
        setImages(viewModel.jsonDataWeekObj.value!!.getString("yourBodyImg"), binding.cardImg3)
        setImages(viewModel.jsonDataWeekObj.value!!.getString("babySizeImg"), binding.cardImg4)
        setImages(viewModel.jsonDataWeekObj.value!!.getString("medicalRecommendationsImg"), binding.cardImg5)
        setImages(viewModel.jsonDataWeekObj.value!!.getString("nextCheckUpImg"), binding.cardImg6)

    }

    private fun setImages(data: String, holder: ImageView){
        Glide.with(requireContext())
            .asBitmap()
            .transform(CenterCrop(), RoundedCorners(context?.resources?.getDimensionPixelSize(R.dimen.cornerRadius)!!))
            .load(data)
            .into(holder)
    }

    private fun setOnClick(){

        binding.card1.setOnClickListener {
            var directions: NavDirections = weekDirections.actionWeekToFeaturedFragment(viewModel.dataObject.value!!)
            findNavController().navigate(directions)
        }

        binding.card2.setOnClickListener {
            var directions: NavDirections = weekDirections.actionWeekToYourBabyFragment(viewModel.dataObject.value!!)
            findNavController().navigate(directions)
        }

        binding.card3.setOnClickListener {
            var directions: NavDirections = weekDirections.actionWeekToYourBodyFragment(viewModel.dataObject.value!!)
            findNavController().navigate(directions)
        }

        binding.card4.setOnClickListener {
            var directions: NavDirections = weekDirections.actionWeekToBabySizeFragment(viewModel.dataObject.value!!)
            findNavController().navigate(directions)
        }

        binding.card5.setOnClickListener {
            var directions: NavDirections = weekDirections.actionWeekToMedicalFragment(viewModel.dataObject.value!!)
            findNavController().navigate(directions)
        }

        binding.card6.setOnClickListener {
            var directions: NavDirections = weekDirections.actionWeekToNextCheckupFragment(viewModel.dataObject.value!!)
            findNavController().navigate(directions)
        }
    }

    fun navigateWeeks(){

        binding.toLeft.setOnClickListener {
            viewModel.navLeft()
            binding.toRight.setImageResource(R.drawable.nav_right)
            if (viewModel.selectedWeek.value == 2) binding.toLeft.setImageResource(R.drawable.nav_left_grey)
            update()
        }

        binding.toRight.setOnClickListener {
            viewModel.navRight()
            binding.toLeft.setImageResource(R.drawable.nav_left)
            if (viewModel.selectedWeek.value == 40) binding.toRight.setImageResource(R.drawable.nav_right_grey)
            update()
        }
    }
}