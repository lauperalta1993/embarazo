package com.laura.miembarazo.Weeks.ViewModelFactory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.laura.miembarazo.DataBase.Person.DAO.PersonDAO
import com.laura.miembarazo.Weeks.weekViewModel


class WeekViewModelFactory(
    private val dataSource: PersonDAO,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(weekViewModel::class.java)) {
            return weekViewModel(dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}