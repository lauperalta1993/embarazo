package com.laura.miembarazo.Profile.View

import android.app.DatePickerDialog
import android.content.Intent
import android.icu.util.Calendar
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.laura.miembarazo.DataBase.Person.Entity.PersonEntity
import com.laura.miembarazo.DataBase.Person.ROOMdb.PersonRoomDatabase
import com.laura.miembarazo.Person
import com.laura.miembarazo.Profile.ViewModel.ProfileViewModel
import com.laura.miembarazo.Profile.ViewModelFactory.ProfileViewModelFactory
import com.laura.miembarazo.R
import com.laura.miembarazo.Weeks.View.WeekActivity
import com.laura.miembarazo.Weeks.ViewModelFactory.WeekViewModelFactory
import com.laura.miembarazo.Weeks.weekViewModel
import com.laura.miembarazo.databinding.FragmentProfileBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class ProfileFragment : Fragment() {

    lateinit var binding : FragmentProfileBinding
    lateinit var viewModel : ProfileViewModel
    lateinit var selectDate: Calendar
    var case : Int = 0

    //lateinit var personData: List<PersonEntity>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {

        //inicializo binding
        binding = FragmentProfileBinding.inflate(layoutInflater)

        //inicializo ViewModel + viewModelFactory
        val application = requireNotNull(this.activity).application

        val dataSource = PersonRoomDatabase.getDatabase(application).personDAO

        val viewModelFactory = ProfileViewModelFactory(dataSource, application)

        viewModel = ViewModelProvider(this, viewModelFactory).get(ProfileViewModel::class.java)

        viewModel.personData.observe(viewLifecycleOwner, Observer { personData ->
            binding.profileName.editText?.setText(personData.name)
            binding.profileFum.text = personData.dateFUM
            binding.profileFpp.editText?.setText(personData.dateFPP)
        })


        //retorno vista
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getPerson()

        binding.profileName.editText?.setText(viewModel.personData.value?.name)
        binding.profileFum.text = viewModel.personData.value?.dateFUM
        binding.profileFpp.editText?.setText(viewModel.personData.value?.dateFPP)


        //funcionamiento del boton calendario

        val formatDate = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            SimpleDateFormat ("dd/MM/YYYY", Locale.US)
        } else {
            TODO("VERSION.SDK_INT < N")
        }

        binding.profileFum.setOnClickListener(View.OnClickListener{
            DatePickerDialog(requireContext(),
                DatePickerDialog.OnDateSetListener { datePicker, year, month, dayOfMonth ->
                selectDate = Calendar.getInstance()
                selectDate.set(Calendar.YEAR,year)
                selectDate.set(Calendar.MONTH,month)
                selectDate.set(Calendar.DAY_OF_MONTH,dayOfMonth)
                viewModel.setDateFUM(selectDate.time)
                viewModel.setFPP(selectDate)
                binding.profileFum.setText(formatDate.format(viewModel.personDateFUM.value?.time))
                binding.profileFpp.editText?.setText (formatDate.format(viewModel.getFPP().time).toString())
                viewModel.setDaysOfPregnancy()
            }
                ,
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)).show()
            // No me gusta esto
            viewModel.setName(binding.profileName.editText?.text.toString())
        })

        //Actualizacion de datos

        binding.btnUpdate.setOnClickListener {

            if (binding.profileName.editText?.text.toString().isNotEmpty() && binding.profileFum.text.toString().isNotEmpty()){
                if(binding.profileFum.text.toString() != viewModel.personData.value!!.dateFUM){
                    if(viewModel.checkpregnancy()) {
                        if (binding.profileName.editText?.text.toString() != viewModel.personData.value!!.name) {
                            case = 1 //Nueva Fecha, verificacion OK. Nuevo Nombre
                        } else if (binding.profileName.editText?.text.toString() == viewModel.personData.value!!.name) {
                            case = 2 // Nueva Fecha, verificacion OK. Igual Nombre
                        }
                    } else {
                        case = 3 //Nueva Fecha, verificacion NOK.
                        Log.d("fum comtent", binding.profileFum.text.toString())
                    }
                } else if(binding.profileFum.text.toString() == viewModel.personData.value!!.dateFUM){
                    if (binding.profileName.editText?.text.toString() != viewModel.personData.value!!.name) {
                        case = 4  //Fecha igual. Nuevo Nombre
                    }
                    else if (binding.profileName.editText?.text.toString() == viewModel.personData.value!!.name) {
                        case = 5 //Fecha igual. Nombre Igual
                    }
                }
            } else {
                case = 6 //Campos Vacios
            }
            Log.d("case",case.toString())
            updateDB(case)
        }
    }

    fun updateDB(case: Int){
        val formatDate = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            SimpleDateFormat ("dd/MM/YYYY", Locale.US)
        } else {
            TODO("VERSION.SDK_INT < N")
        }
        when(case) {
            1 -> //Actualiza Nombre. Actualiza fecha FUM. ViewModel.checkPregnancy = true
            {
                val person = PersonEntity(
                    viewModel.personId,
                    name = binding.profileName.editText!!.text.toString(),
                    dateFUM = binding.profileFum.text.toString(),
                    dateFPP = formatDate.format(viewModel.getFPP().time).toString(),
                    daysOfPregnancy = viewModel.getDaysOfPregnancy(),
                    weeksOfPregnancy = viewModel.getWeeksOfPregnancy(),
                    addDaysofPregnancy = viewModel.getAddDaysOfPregnancy(),
                    thisWeekDateInit = formatDate.format(viewModel.getThisWeekInit()?.time).toString(),
                    thisWeekDateEnd = formatDate.format(viewModel.getThisWeekEnd()?.time).toString()
                )
                viewModel.updatePerson(person)
                Toast.makeText(requireContext(),"Datos Actualizados correctamente",Toast.LENGTH_SHORT).show()
            }
            2 -> //No actualiza Nombre. Actualiza fecha FUM. ViewModel.checkPregnancy = true
            {
                val person = PersonEntity(
                    viewModel.personId,
                    name = viewModel.personData.value!!.name,
                    dateFUM = binding.profileFum.text.toString(),
                    dateFPP = formatDate.format(viewModel.getFPP().time).toString(),
                    daysOfPregnancy = viewModel.getDaysOfPregnancy(),
                    weeksOfPregnancy = viewModel.getWeeksOfPregnancy(),
                    addDaysofPregnancy = viewModel.getAddDaysOfPregnancy(),
                    thisWeekDateInit = formatDate.format(viewModel.getThisWeekInit()?.time).toString(),
                    thisWeekDateEnd = formatDate.format(viewModel.getThisWeekEnd()?.time).toString()
                )
                viewModel.updatePerson(person)
                Toast.makeText(requireContext(),"Fecha Actualizada correctamente",Toast.LENGTH_SHORT).show()
            }
            3 -> //Actualiza Fecha FUM. ViewModel.checkPregnancy = false
            {
                Toast.makeText(requireContext(),"Por favor ingrese una fecha valida",Toast.LENGTH_SHORT).show()
            }
            4 -> //Actualiza Nombre. No actualiza Fecha FUM
            {
                val person = PersonEntity(
                    viewModel.personId,
                    name = binding.profileName.editText?.text.toString(),
                    dateFUM = viewModel.personData.value!!.dateFUM,
                    dateFPP = viewModel.personData.value!!.dateFPP,
                    daysOfPregnancy = viewModel.personData.value!!.daysOfPregnancy,
                    weeksOfPregnancy = viewModel.personData.value!!.weeksOfPregnancy,
                    addDaysofPregnancy = viewModel.personData.value!!.addDaysofPregnancy,
                    thisWeekDateInit = viewModel.personData.value!!.thisWeekDateInit,
                    thisWeekDateEnd = viewModel.personData.value!!.thisWeekDateEnd,
                )
                viewModel.updatePerson(person)
                Toast.makeText(requireContext(),"Nombre Actualizado correctamente",Toast.LENGTH_SHORT).show()
            }
            5 -> //No hay variacion en los datos
            {
                Toast.makeText(requireContext(),"Ningun dato se ha modificado",Toast.LENGTH_SHORT).show()
            }
            6 -> //Campos incompletos
            {
                Toast.makeText(requireContext(),"Por favor complete los campos requeridos",Toast.LENGTH_SHORT).show()
            }
        }
        viewModel.getPerson()

    }
}



