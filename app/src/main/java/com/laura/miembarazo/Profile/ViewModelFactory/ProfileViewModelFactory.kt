package com.laura.miembarazo.Profile.ViewModelFactory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.laura.miembarazo.DataBase.Person.DAO.PersonDAO
import com.laura.miembarazo.Profile.ViewModel.ProfileViewModel
import com.laura.miembarazo.Weeks.weekViewModel


class ProfileViewModelFactory(
    private val dataSource: PersonDAO,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ProfileViewModel::class.java)) {
            return ProfileViewModel(dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}